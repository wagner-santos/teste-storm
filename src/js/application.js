function openMenu() {
  var menu = document.getElementsByClassName("menu__dropdown")[0];
  var image = document.getElementsByClassName("menu__media-image")[0];
  if (menu.style.display === "block") {
    menu.style.display = "none";
    image.src = "assets/images/menu.png";
  } else {
    menu.style.display = "block";
    image.src = "assets/images/open_menu.png";
  }
}

function menuActive(element) {
	var param = document.getElementsByClassName('active');
	while(param.length > 0) {
		param[0].classList.remove("active");
	}​
	var el = element.closest('li');
	el.classList.add("active");
}

function menuActiveDesk(element) {
	var param = document.getElementsByClassName('active');
	var el = element.closest('li');
	while(param.length > 0) {
		param[0].classList.remove("active");
	}​
	el.classList.add("active");
}

function selectNews(element) {
	var param = document.getElementsByClassName('selected');
	var el = element.closest('li');
	while(param.length > 0) {
		param[0].classList.remove("selected");
	}​
	el.classList.add("selected");
}

document.getElementsByClassName("footer__form__submit")[0].onclick = function(){
	var name = document.getElementsByClassName("footer__form__input")[0].value;
	var email = document.getElementsByClassName("footer__form__input")[1].value;
	var msg = document.getElementsByClassName("footer__form__textarea")[0].value;

	if(name == ''|| email == '' || msg == ''){
		alert('Preencha os campos');
		return false;
	}
	if(name  == name.match(/[^a-zA-Z]+/)){
		alert("Somente letras");
		return false;
	}
	if(msg.length < 4 ){
		alert('Escreva uma mensagem maior que 3 caracteres');
		return false;
	}

	alert("Sua mensagem foi enviada com sucesso.");

	document.getElementsByClassName("footer__form__input")[0].value = '';
	document.getElementsByClassName("footer__form__input")[1].value = '';
	document.getElementsByClassName("footer__form__textarea")[0].value = '';
	return true;
}

$('.slider').slick({
  infinite: false
});
