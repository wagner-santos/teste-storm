var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var bs = require('browser-sync');
var order = require('run-sequence');
var ssi = require('gulp-ssi');
var dom = require('gulp-dom');
var wait = require('gulp-wait');
var flatmap = require('gulp-flatmap');
var net = require('net');
var smushit = require('gulp-smushit');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var del = require('del');

var portNumber = 3000;


function getPort(portNumber){
    return new Promise(function(resolve, reject){
        var server = net.createServer(function(socket) {
            socket.write('Echo server\r\n');
            socket.pipe(socket);
        });

        server.listen(portNumber, '127.0.0.1');

        server.on('error', function (e) {
            getPort(portNumber + 2);
        });
        server.on('listening', function (e) {
            server.close();
            resolve(portNumber);
        });
    });
}

// development
gulp.task('styles', function() {
    return gulp.src('./src/sass/style.scss')
        .pipe(sourcemaps.init())
        .pipe(
            sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(concat('application.css'))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest('./server/css'));
});

gulp.task('script', function() {
    return gulp.src('./src/js/*.js')
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(gulp.dest('./server/js'));
});

gulp.task('html', function(){
    return gulp.src('src/*.html')
        .pipe(flatmap(function(stream, file){
            return stream
                .pipe(ssi())
        }))
        .pipe(gulp.dest(`server`));
});

gulp.task('smushit', function () {
    return gulp.src('./src/assets/**/*')
        .pipe(smushit({
            verbose: true
        }))
        .pipe(gulp.dest('./server/assets/'));
});

gulp.task('clean-server', function(){
    return del([
        'server/assets/*',
        'server/css/*',
        'server/js/*'
    ]);
});

// Browsersyncing
gulp.task('bsync', function(){
    getPort(portNumber).then((port) => {
        bs.init({
            server : {
                baseDir: `./server/`,
                directory: false,
                port: port
            }
        });
    });
    gulp.watch('src/sass/**/*.scss', ['styles']);
    gulp.watch('src/sass/**/*.scss').on('change', bs.reload);

    gulp.watch('src/js/**/*.js', ['script']);
    gulp.watch('src/js/**/*.js').on('change', bs.reload);

    gulp.watch('src/*.html', ['dist-refresh-html']);
    gulp.watch('src/html-modules/**/*.html', ['dist-refresh-html']); 

    gulp.watch('src/assets/**',['assets']);
    gulp.watch('src/assets/**').on('change', bs.reload);

    

});

gulp.task('dist-refresh-html', function(){
    order('html', 'reload');
});

gulp.task('reload', function(){
    bs.reload();
});

gulp.task('server', function(){
    order(
        'clean-server',
        'styles', 
        'script',  
        'smushit',
        'html',
        'bsync'
    );
});