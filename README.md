
## Teste Storm
* Teste para desenvoledor front-end

### Instalação
* Clonar o projeto
* Instalar o npm

### Executando
* Executar comando gulp server no prompt

### Tecnologia utilizada 
* HTML
* CSS
* SASS
* JavaScript
* Jquery
* Gulp
* Git

### Autor
Wagner Santos


